<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Material;
class MaterialController extends Controller
{
    
    public function index()
    {
        return view('layouts.materiales.material');
    }

    public function create()
    {
        return view('layouts.materiales.create');
    }

    public function store(Request $request)
    {
        
        $request->validate([

            'name'=> 'required',
            'quantity'=> 'required',
            'price'=> 'required',
            'Tipo de Material'=> 'required' 
           
        ]);

        Material::create($request->all());
        return redirect()->route('materiales.material')->with('success', 'Product created successfully.');

        dd($request);
    }

    
}

