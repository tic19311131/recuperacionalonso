<?php

namespace Database\Factories;

use App\Models\Obras;
use Illuminate\Database\Eloquent\Factories\Factory;

class ObrasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Obras::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
