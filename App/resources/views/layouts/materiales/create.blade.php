<form action="{{url('/materiales/create')}}" method="post" enctype="multipart/form-data">
{{ csrf_field() }}

<label for ="name">{{'Nombre'}}</label>
<input type="text" name="name" id="name" value>
<br>
<label for ="quantity">{{'Cantidad'}}</label>
<input type="text" name="quantity" id="quantity" value>
<br>
<label for ="price">{{'Precio'}}</label>
<input type="text" name="price" id="price" value>
<br>
<label for ="material_type">{{'Tipo de Material'}}</label>
<select name="material_type">
  <option value="CIVIL">Civil</option>
  <option value="ELECTRICO" selected>Electrico</option>
 
</select>


<br>
<input type="submit" value="Agregar">

</form>

